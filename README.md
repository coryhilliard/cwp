# This is a walkthrough to get CWP installed and updated on a DigitalOcean CentOS7 Droplet

I am giving this work away free as I'm a huge believer in leapfrogging over the work of others to build greater things.

## License: GNU GPL v3.0

[![GNU GPL v3.0](http://www.gnu.org/graphics/gplv3-127x51.png)](http://www.gnu.org/licenses/gpl.html)

GNU site <http://www.gnu.org/licenses/gpl.html>.
